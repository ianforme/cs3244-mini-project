import pandas as pd
import datetime
import csv
import numpy as np
import os
import scipy as sp
import xgboost as xgb
import lightgbm as lgb
import itertools
import operator
import warnings
import random
warnings.filterwarnings("ignore")

from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.base import TransformerMixin
from sklearn import cross_validation
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_predict


import preprocess

random.seed(3244)

features, features_non_numeric = preprocess.main()

OUTPUT_TEST_FILE_PATH = "./data/test_clean_1.csv"
OUTPUT_TRAIN_FILE_PATH = "./data/train_clean_1.csv"
OUTPUT_TRAIN_TEST_PATH     = "./data/train_set.csv"
OUTPUT_VALIDATION_PATH = "./data/train_validation.csv"

train_full = pd.read_csv(OUTPUT_TRAIN_FILE_PATH)
train = pd.read_csv(OUTPUT_TRAIN_TEST_PATH)
valid = pd.read_csv(OUTPUT_VALIDATION_PATH)
test = pd.read_csv(OUTPUT_TEST_FILE_PATH)

#predefined RMSPE evaluation function
def ToWeight(y):
	w = np.zeros(y.shape, dtype=float)
	ind = y != 0
	w[ind] = 1./(y[ind]**2)
	return w

def rmspe(yhat, y):
	w = ToWeight(y)
	rmspe = np.sqrt(np.mean( w * (y - yhat)**2 ))
	return rmspe

def rmspe_xg(yhat, y):
	# y = y.values
	y = y.get_label()
	y = np.exp(y) - 1
	yhat = np.exp(yhat) - 1
	w = ToWeight(y)
	rmspe = np.sqrt(np.mean(w * (y - yhat)**2))
	return "rmspe", rmspe

goal = "Sales"
myid = "Id"



def XGB_native(train,valid,test,features):
	"""
	Build a new xgboost model and train a xgboost model

	Arguments:
		train: dataframe
            train data
        valid: dataframe
            validation data
		test: dataframe
            test data
		features: list
			list of columns to choose to train
	"""
	depth = 12
	eta = 0.01
	ntrees = 2500
	mcw = 3
	params = {"objective": "reg:linear",
			  "booster": "gbtree",
			  "eta": eta,
			  "max_depth": depth,
			  "min_child_weight": mcw,
			  "subsample": 0.8,
			  "colsample_bytree": 0.7,
			  "silent": 1
			  }
	print ("Running with params: " + str(params))
	print ("Running with ntrees: " + str(ntrees))
	print ("Running with features: " + str(features))

	dtrain = xgb.DMatrix(train[features], np.log(train[goal] + 1))
	dvalid = xgb.DMatrix(valid[features], np.log(valid[goal] + 1))
	watchlist = [(dvalid, 'eval'), (dtrain, 'train')]
	gbm = xgb.train(params, dtrain, ntrees, evals=watchlist, early_stopping_rounds=250, feval=rmspe_xg, verbose_eval=250)
	train_probs = gbm.predict(xgb.DMatrix(valid[features]))
	indices = train_probs < 0
	train_probs[indices] = 0
	error = rmspe(np.exp(train_probs) - 1, valid[goal].values)
	print(error)

	# Predict and Export
	test_probs = gbm.predict(xgb.DMatrix(test[features]))
	indices = test_probs < 0
	test_probs[indices] = 0
	indices = list(test[myid][test['Open']==0])
	indices = [int(i) for i in indices]
	submission = pd.DataFrame({myid: test[myid], goal: np.exp(test_probs) - 1})
	submission[goal][submission[myid].isin(indices)] = 0
	if not os.path.exists('result/'):
		os.makedirs('result/')
	submission.to_csv("./result/dat-xgb_d%s_eta%s_ntree%s_mcw%s.csv" % (str(depth),str(eta),str(ntrees),str(mcw)) , index=False)
	return train_probs,test_probs,error

def XGB_native_multiple(train_df,test,frac,num_models,features):
	"""
	Build multiple new xgboost models by resampling the train dataset by frac and train a xgboost model, select the best performace model on validation set to predict

	Arguments:
		train_df: dataframe
            train data
		test: dataframe
            test data
        frac: float
            fraction of train data
		num_models: int
			number of xgboost models to build
		features: list
			list of columns to choose to train
	"""
	depth = 12
	eta = 0.01
	ntrees = 1500
	mcw = 3
	params = {"objective": "reg:linear",
			  "booster": "gbtree",
			  "eta": eta,
			  "max_depth": depth,
			  "min_child_weight": mcw,
			  "subsample": 0.8,
			  "colsample_bytree": 0.7,
			  "silent": 1
			  }
	print ("Running with params: " + str(params))
	print ("Running with ntrees: " + str(ntrees))
	print ("Running with features: " + str(features))

	error = 1
	gbm = 0
	for i in range(num_models):
		train = train_df.sample(frac = frac)
		valid = train_df.drop(train.index)
		dtrain = xgb.DMatrix(train[features], np.log(train[goal] + 1))
		dvalid = xgb.DMatrix(valid[features], np.log(valid[goal] + 1))
		watchlist = [(dvalid, 'eval'), (dtrain, 'train')]
		tmp_gbm = xgb.train(params, dtrain, ntrees, evals=watchlist, early_stopping_rounds=250, feval=rmspe_xg, verbose_eval=250)
		train_probs = tmp_gbm.predict(xgb.DMatrix(valid[features]))
		indices = train_probs < 0
		train_probs[indices] = 0
		tmp_error = rmspe(np.exp(train_probs) - 1, valid[goal].values)
		if (tmp_error < error):
			error = tmp_error
			gbm = tmp_gbm
		print(error)

	# Predict and Export
	test_probs = gbm.predict(xgb.DMatrix(test[features]))
	indices = test_probs < 0
	test_probs[indices] = 0
	indices = list(test[myid][test['Open']==0])
	indices = [int(i) for i in indices]
	submission = pd.DataFrame({myid: test[myid], goal: np.exp(test_probs) - 1})
	submission[goal][submission[myid].isin(indices)] = 0
	if not os.path.exists('result/'):
		os.makedirs('result/')
	submission.to_csv("./result/dat-xgb_d%s_eta%s_ntree%s_mcw%s_%s.csv" % (str(depth),str(eta),str(ntrees),str(mcw),str(datetime.datetime.now().date())) , index=False)

def lightGBM_native(train, valid, test, features):
	"""
	Build a new lightgbm model 

	Arguments:
		train_df: dataframe
            train data
		test: dataframe
            test data
        frac: float
            fraction of train data
		num_models: int
			number of xgboost models to build
		features: list
			list of columns to choose to train
	"""
	param = {'objective': 'regression',
		'boosting_type': 'gbdt',
		'metric': {'l2', 'auc'},
		'learning_rate': 0.05,
		'num_leaves': 256,
		'max_depth': 13,
		'feature_fraction': 0.9,
		'bagging_fraction': 0.8,
		'bagging_freq': 3,
	}

	num_round = 2500

	train_data = lgb.Dataset(train[features], np.log(train[goal] + 1))
	valid_data = lgb.Dataset(valid[features], np.log(valid[goal] + 1))

	bst = lgb.train(param,
					train_data,
					num_round,
					valid_sets=valid_data
					)
	train_probs = bst.predict(valid[features])
	error = rmspe(np.exp(train_probs) - 1, valid[goal].values)
	# Predict and Export
	test_probs = bst.predict(test[features])
	indices = test_probs < 0
	test_probs[indices] = 0
	indices = list(test[myid][test['Open']==0])
	indices = [int(i) for i in indices]
	submission = pd.DataFrame({myid: test[myid], goal: np.exp(test_probs) - 1})
	submission[goal][submission[myid].isin(indices)] = 0
	if not os.path.exists('result/'):
		os.makedirs('result/')
	submission.to_csv("./result/dat-lbg_" + str(datetime.datetime.now()) + ".csv" , index=False)
	return train_probs,test_probs,error

def train_weight (model1_train_probs,model2_train_probs,model1_test_probs,model2_test_probs,valid,test):
	"""
	Adjust the weight of predictions from two models to generate predictions with smaller rmspe value on validation set

	Arguments:
		model1_train_probs: dataframe
			validation prediction from model 1
		model2_train_probs: dataframe
			validation prediction from model 2
		model1_test_probs： dataframe
			test set prediction from model 1
		model2_test_probs： dataframe
			test set prediction from model 2		
		valid: dataframe
            validation data
		test: dataframe
            test data
	"""
	#initialize weight and rmspe error
	a = 0
	b = 0
	min_rmspe = 100000

	#trian for 10000 rounds
	for i in range(50000):
		w1 = random.random()
		w2 = 1- random.random()
		tmp_prob = w1 * model1_train_probs + w2 * model2_train_probs
		tmp_error = rmspe(np.exp(tmp_prob) - 1, valid[goal].values)
		if (tmp_error < min_rmspe):
			a = w1
			b = w2
			min_rmspe = tmp_error
	print(min_rmspe)
	print("weight train complete")
	#write to csv
	test_probs = a*model1_test_probs + b*model2_test_probs
	indices = list(test[myid][test['Open']==0])
	indices = [int(i) for i in indices]
	submission = pd.DataFrame({myid: test[myid], goal: np.exp(test_probs) - 1})
	submission[goal][submission[myid].isin(indices)] = 0
	if not os.path.exists('result/'):
		os.makedirs('result/')
	submission.to_csv("./result/ensemble_" + str(datetime.datetime.now()) + ".csv" , index=False)
	return a,b, min_rmspe






if __name__ == '__main__':
	#ensemble from one xgboost model and one lightgbm model
	xbg_train,xbg_test,xbg_error = XGB_native(train, valid, test, features)
	gbm_train,gbm_test,gbm_error = lightGBM_native(train, valid, test, features)
	w1,w2,emsemble_error = train_weight(xbg_train,gbm_train,xbg_test,gbm_test,valid,test)

	#multiple xgboost model training
	XGB_native_multiple(train_full,test,0.9,5,features)
	