import pandas as pd
import datetime
import csv
import numpy as np
import os
import scipy as sp
import xgboost as xgb
import lightgbm as lgb
import itertools
import operator
import warnings
import random
warnings.filterwarnings("ignore")

from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.base import TransformerMixin
from sklearn import cross_validation
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_predict


import preprocess

random.seed(3244)

OUTPUT_TRAIN_FILE_PATH = "./data/train_clean.csv"
OUTPUT_TEST_FILE_PATH = "./data/test_clean.csv"
OUTPUT_TRAIN_TEST_PATH     = "./data/train_set.csv"
OUTPUT_VALIDATION_PATH = "./data/train_validation.csv"

train = pd.read_csv(OUTPUT_TRAIN_TEST_PATH)
valid = pd.read_csv(OUTPUT_VALIDATION_PATH)
test = pd.read_csv(OUTPUT_TEST_FILE_PATH)

def ToWeight(y):
	w = np.zeros(y.shape, dtype=float)
	ind = y != 0
	w[ind] = 1./(y[ind]**2)
	return w

def rmspe(yhat, y):
	w = ToWeight(y)
	rmspe = np.sqrt(np.mean( w * (y - yhat)**2 ))
	return rmspe

def rmspe_xg(yhat, y):
	# y = y.values
	y = y.get_label()
	y = np.exp(y) - 1
	yhat = np.exp(yhat) - 1
	w = ToWeight(y)
	rmspe = np.sqrt(np.mean(w * (y - yhat)**2))
	return "rmspe", rmspe

goal = "Sales"
myid = "Id"

features, features_non_numeric = preprocess.main()

#baseline OLS model
def OLS(train,valid,test,features,features_non_numeric):
	olsReg = LinearRegression()
	#combing train and validation set
	X_train = train[features].append(valid[features])
	y_train = train[goal].append(valid[goal])
	olsReg.fit(X_train,y_train)

	#cross validated prediction
	predicted = cross_val_predict(olsReg, X_train, y_train, cv=10)

	#predicted = olsReg.predict(X_val)
	error = rmspe(predicted, y_train.values)
	print(error) #error = 0.219942007682

	#prediction
	X_test = test[features]
	test_probs = olsReg.predict(X_test)
	indices = list(test[myid][test['Open']==0])
	indices = [int(i) for i in indices]
	submission = pd.DataFrame({myid: test[myid], goal: test_probs})
	submission[goal][submission[myid].isin(indices)] = 0

	if not os.path.exists('result/'):
		os.makedirs('result/')
	submission.to_csv("./result/dat-baselineOLS.csv",index= False)





def XGB_native(train,valid,test,features,features_non_numeric,feature_sample_size,model_size):
	depth = 13
	eta = 0.01
	ntrees = 2500
	mcw = 3
	params = {"objective": "reg:linear",
			  "booster": "gbtree",
			  "eta": eta,
			  "max_depth": depth,
			  "min_child_weight": mcw,
			  "subsample": 0.9,
			  "colsample_bytree": 0.7,
			  "silent": 1
			  }
	print ("Running with params: " + str(params))
	print ("Running with ntrees: " + str(ntrees))
	print ("Running with features: " + str(features))

	# Train model with local split
	# tsize = 0.05
	# X_train, X_test = cross_validation.train_test_split(train, test_size=tsize)

	
	error = 0.06
	gbm = None
	for i in range(model_size):
		sub_features = np.random.choice(features,feature_sample_size,replace = False)
		dtrain = xgb.DMatrix(train[sub_features], np.log(train[goal] + 1))
		dvalid = xgb.DMatrix(valid[sub_features], np.log(valid[goal] + 1))
		watchlist = [(dvalid, 'eval'), (dtrain, 'train')]
		gbm_tmp = xgb.train(params, dtrain, ntrees, evals=watchlist, early_stopping_rounds=100, feval=rmspe_xg, verbose_eval=True)
		train_probs = gbm_tmp.predict(xgb.DMatrix(valid[sub_features]))
		indices = train_probs < 0
		train_probs[indices] = 0
		tmp_error = rmspe(np.exp(train_probs) - 1, valid[goal].values)
		print(tmp_error)
		if (tmp_error < error):
			error = tmp_error
			gbm = gbm_tmp
			feature_list = sub_features
	
	if (gbm != None):
	# Predict and Export
		test_probs = gbm.predict(xgb.DMatrix(test[feature_list]))
		indices = test_probs < 0
		test_probs[indices] = 0
		indices = list(test[myid][test['Open']==0])
		indices = [int(i) for i in indices]
		submission = pd.DataFrame({myid: test[myid], goal: np.exp(test_probs) - 1})
		submission[goal][submission[myid].isin(indices)] = 0
		if not os.path.exists('result/'):
			os.makedirs('result/')
		submission.to_csv("./result/dat-xgb_d%s_eta%s_ntree%s_mcw%s"% (str(depth),str(eta),str(ntrees),str(mcw))  +str(datetime.datetime.now())+".csv" , index=False)
		return train_probs,test_probs,error
	else:
		return 0,0,0

def lightGBM_native(train, valid, test, features):
	param = {'objective': 'regression',
		'boosting_type': 'gbdt',
		'metric': {'l2', 'auc'},
		'learning_rate': 0.05,
		'num_leaves': 256,
		'max_depth': 13,
		'feature_fraction': 0.9,
		'bagging_fraction': 0.8,
		'bagging_freq': 3,
	}

	num_round = 3000

	train_data = lgb.Dataset(train[features], np.log(train[goal] + 1))
	valid_data = lgb.Dataset(valid[features], np.log(valid[goal] + 1))

	bst = lgb.train(param,
					train_data,
					num_round,
					valid_sets=valid_data
					)
	train_probs = bst.predict(valid[features])
	error = rmspe(np.exp(train_probs) - 1, valid[goal].values)
	# Predict and Export
	test_probs = bst.predict(test[features])
	indices = test_probs < 0
	test_probs[indices] = 0
	indices = list(test[myid][test['Open']==0])
	indices = [int(i) for i in indices]
	submission = pd.DataFrame({myid: test[myid], goal: np.exp(test_probs) - 1})
	submission[goal][submission[myid].isin(indices)] = 0
	if not os.path.exists('result/'):
		os.makedirs('result/')
	submission.to_csv("./result/dat-lbg_" + str(datetime.datetime.now()) + ".csv" , index=False)
	return train_probs,test_probs,error

def train_weight (model1_train_probs,model2_train_probs,model1_test_probs,model2_test_probs,valid,test):
	#initialize weight and rmspe error
	a = 0
	b = 0
	min_rmspe = 10000

	#train for 10000 rounds
	for i in range(10000):
		w1 = random.random()
		w2 = 1- random.random()
		tmp_prob = w1 * model1_train_probs + w2 * model2_train_probs
		tmp_error = rmspe(np.exp(tmp_prob) - 1, valid[goal].values)
		if (tmp_error < min_rmspe):
			a = w1
			b = w2
			min_rmspe = tmp_error
	print(tmp_error)
	print("weight train complete")
	#write to csv
	test_probs = a*model1_test_probs + b*model2_test_probs
	indices = list(test[myid][test['Open']==0])
	indices = [int(i) for i in indices]
	submission = pd.DataFrame({myid: test[myid], goal: np.exp(test_probs) - 1})
	submission[goal][submission[myid].isin(indices)] = 0
	if not os.path.exists('result/'):
		os.makedirs('result/')
	submission.to_csv("./result/ensemble_" + str(datetime.datetime.now()) + ".csv" , index=False)
	return a,b, min_rmspe






if __name__ == '__main__':
	xbg_train,xbg_test,xbg_error = XGB_native(train, valid, test, features, features_non_numeric,15,5)
	#gbm_train,gbm_test,gbm_error = lightGBM_native(train, valid, test, features)
	#OLS(train,valid,test,features,features_non_numeric)
	#w1,w2,emsemble_error = train_weight(xbg_train,gbm_train,xbg_test,gbm_test,valid,test)
	#train_weight(xbg_train,gbm_train,xbg_test,gbm_test,valid,test)

