import pandas as pd
import numpy as np
import datetime
import csv
import os
import scipy as sp
import itertools
import operator
import warnings
import random
warnings.filterwarnings("ignore")
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.base import TransformerMixin
from sklearn import cross_validation

import feature_engineering

INPUT_TRAIN_FILE_PATH = "./data/train_clean.csv"
INPUT_TEST_FILE_PATH = "./data/test_clean.csv"
INPUT_STORE_FILE_PATH ="./data/store.csv"
OUTPUT_TRAIN_FILE_PATH = "./data/train_clean_1.csv"
OUTPUT_TEST_FILE_PATH = "./data/test_clean_1.csv"
OUTPUT_TRAIN_TEST_PATH     = "./data/train_set.csv"
OUTPUT_VALIDATION_PATH = "./data/train_validation.csv"
myid = 'Id'

random.seed(3244)

feature_engineering.process("./data/train.csv", "./data/test.csv", INPUT_TRAIN_FILE_PATH, INPUT_TEST_FILE_PATH)

def load_data(input_train_path,input_test_path,input_store_path):
	"""
		Load data and specified features of the data sets
	"""
	store = pd.read_csv(input_store_path, dtype={'PromoInterval':pd.np.string_})
	train_raw = pd.read_csv(input_train_path,dtype={'StateHoliday':pd.np.string_})
	test_raw = pd.read_csv(input_test_path,dtype={'StateHoliday':pd.np.string_})

	#merge train, test dataset with store by id = "Store"
	train = pd.merge(train_raw,store, on='Store', how='left')
	test = pd.merge(test_raw,store, on='Store', how='left')
	
	# Add id column to train and test, and fill all NA value by 0
	for data in [train,test]:
		data[myid] = range(1, len(data)+1)
		data.fillna(0, inplace = True)
	
	features = test.columns.tolist()
	numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
	#select numeric dtype features column names
	features_numeric = test.select_dtypes(include=numerics).columns.tolist()
	#select non-numeric dtype features column names
	features_non_numeric = [f for f in features if f not in features_numeric]

	return (train,test,features,features_non_numeric)


def process_data(train,test,features,features_non_numeric,output_train_file_path,output_test_file_path):
	"""
		Feature engineering and selection.
	"""
	# FEATURE ENGINEERING
	#train = train[train['Sales'] > 0]

	for data in [train,test]:
		# year month day
		data['year'] = data.Date.apply(lambda x: x.split('-')[0])
		data['year'] = data['year'].astype(float)
		data['month'] = data.Date.apply(lambda x: x.split('-')[1])
		data['month'] = data['month'].astype(float)
		data['day'] = data.Date.apply(lambda x: x.split('-')[2])
		data['day'] = data['day'].astype(float)
		

		# convert competitionOpenSinceMonth, competitionOpenSinceYear to comeptition period
		# difference between these two with date in number of month
		data['competitionTime'] = (data['year'] - data['CompetitionOpenSinceYear'])*12 + data['month'] - data["CompetitionOpenSinceMonth"]

		# assume one year has 52 weeks, one month 4 weeks
		data['promotionTime'] = (data['year'] - data['Promo2SinceYear']) * 52 + data['month']*4 - data['Promo2SinceWeek']
	
	# Pre-processing non-numeric values
	# le = LabelEncoder()
	# for col in features_non_numeric:
	# 	le.fit(list(set(train[col]))+list(set(test[col])))
	# 	train[col] = le.transform(train[col].astype(str))
	# 	test[col] = le.transform(test[col].astype(str))

	# Convert categorical data into 1 and 0s
	# state holiday - 0, a, b, c
	# store type - a, b, c, d
	# assortment - a, b, c
	# # PromoInterval

	train = pd.concat([train, pd.get_dummies(train.DayOfWeek, prefix = "DayOfWeek")], axis=1)
	train = pd.concat([train, pd.get_dummies(train.StateHoliday, prefix = "StateHoliday")], axis=1)
	train = pd.concat([train, pd.get_dummies(train.StoreType, prefix = "StoreType")], axis=1)
	train = pd.concat([train, pd.get_dummies(train.Assortment, prefix = "Assortment")], axis=1)
	train = pd.concat([train, pd.get_dummies(train.PromoInterval, prefix = "PromoInterval")], axis=1)

	test = pd.concat([test, pd.get_dummies(test.DayOfWeek, prefix = "DayOfWeek")], axis=1)
	test = pd.concat([test, pd.get_dummies(test.StateHoliday, prefix = "StateHoliday")], axis=1)
	test = pd.concat([test, pd.get_dummies(test.StoreType, prefix = "StoreType")], axis=1)
	test = pd.concat([test, pd.get_dummies(test.Assortment, prefix = "Assortment")], axis=1)
	test = pd.concat([test, pd.get_dummies(test.PromoInterval, prefix = "PromoInterval")], axis=1)

	test["StateHoliday_a"] = 0
	test["StateHoliday_c"] = 0
	#scale data for better performance on tree based models such as xgboost
	scaler = StandardScaler()
	for col in ['DayOfWeek','Customers', 'CompetitionDistance', 'competitionTime', 'promotionTime', 'Store','Store_median_sales','store_dayofweek_median_sales','dayofweek_median_sales','sales_customer_ratio','non_school_holiday_median_sales_per_store','school_holiday_median_sales_per_store','SchoolHoliday_NonSchoolHoliday_median_sales','non_state_holiday_median_sales_per_store','state_holiday_median_sales_per_store',
		'StateHoliday_NonStateHoliday_median_sales', 'Customers_per_day', 'Customers_per_dow', 'Customers_per_store', 'Customers_per_state_holiday', 'Customers_per_school_holiday', 
		'next_7_day_holidays']:
		scaler.fit(np.reshape(list(train[col])+list(test[col]), (-1,1)))
		train[col] = scaler.transform(np.reshape(train[col], (-1,1)))
		test[col] = scaler.transform(np.reshape(test[col], (-1,1)))

	# Features set.
	new = ['day', 'month', 'year', 'competitionTime', 'promotionTime', 'StateHoliday_0', 'StateHoliday_a', 'StateHoliday_c', 'StoreType_a',
		'StoreType_b', 'StoreType_c', 'StoreType_d', 'Assortment_a', 'Assortment_b', 'Assortment_c', 'PromoInterval_0', 'PromoInterval_Feb,May,Aug,Nov',
		'PromoInterval_Jan,Apr,Jul,Oct', 'PromoInterval_Mar,Jun,Sept,Dec', 'DayOfWeek_1', 'DayOfWeek_2', 'DayOfWeek_3', 'DayOfWeek_4', 
		'DayOfWeek_5', 'DayOfWeek_6', 'DayOfWeek_7']
	features.extend(new)
	noisy_features = [myid, 'Date', 'CompetitionOpenSinceMonth', 'CompetitionOpenSinceYear', 'Promo2SinceYear', 'Promo2SinceWeek', 'day', 'month', 'year', 
		'StateHoliday', 'StoreType', 'Assortment', 'PromoInterval', 'DayOfWeek']
	features = [c for c in features if c not in noisy_features]
	features_non_numeric = [c for c in features_non_numeric if c not in noisy_features]
	#remove noisy features from train and test dataset
	noisy_features.remove(myid)
	train = train.drop(noisy_features,axis = 1)
	test = test.drop(noisy_features,axis = 1)
	#output cleaned train and test dataset
	train.to_csv(output_train_file_path,index=False)
	test.to_csv(output_test_file_path,index=False)
    #print("File cleaned and written to {}".format(output_train_file_path))
    #print("File cleaned and written to {}".format(output_test_file_path))

	return (train,test,features,features_non_numeric)

def split_train_set(train_df, frac, output_train_file_path,output_train_test_file_path, output_validation_file_path):
    """Splits train set into training and validation set

    Arguments:
        train_df: dataframe
            train data

        frac: float
            fraction of train data

        output_train_file_path: String
            main train set will be written to this file path

        output_validation_file_path: String
            validation set will be written to this file path
    """
    train_df.to_csv(output_train_file_path,index=False)
    train = train_df.sample(frac = frac)
    train.to_csv(output_train_test_file_path,index=False)
    validation = train_df.drop(train.index)
    validation.to_csv(output_validation_file_path,index=False)

def main():
	print('start')
	train_df,test_df,features,features_non_numeric = load_data(INPUT_TRAIN_FILE_PATH,INPUT_TEST_FILE_PATH,INPUT_STORE_FILE_PATH)
	train,test,features,features_non_numeric = process_data(train_df,test_df,features,features_non_numeric,OUTPUT_TRAIN_FILE_PATH,OUTPUT_TEST_FILE_PATH)
	split_train_set(train,0.9,OUTPUT_TRAIN_FILE_PATH,OUTPUT_TRAIN_TEST_PATH,OUTPUT_VALIDATION_PATH)
	print("complete")
	return (features,features_non_numeric)

if __name__ == '__main__':
	main()
