import pandas as pd
import numpy as np
import datetime
import csv
import os
import scipy as sp
import itertools
import operator
import warnings
import random
warnings.filterwarnings("ignore")
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.base import TransformerMixin
from sklearn import cross_validation

INPUT_TRAIN_FILE_PATH = "./data/train.csv"
INPUT_TEST_FILE_PATH = "./data/test.csv"
OUTPUT_TRAIN_FILE_PATH = "./data/train_clean.csv"
OUTPUT_TEST_FILE_PATH = "./data/test_clean.csv"

random.seed(3244)

def process(input_train_file_path,input_test_file_path,output_train_file_path,output_test_file_path):

	train_raw = pd.read_csv(input_train_file_path,dtype={'StateHoliday':pd.np.string_})
	test_raw = pd.read_csv(input_test_file_path,dtype={'StateHoliday':pd.np.string_})
	#print(train_raw.columns.tolist())

	#calcualte store median sales
	store_median_sales = train_raw.groupby('Store')[['Sales']].median().reset_index()
	store_median_sales.columns = ["Store","Store_median_sales"]
	train = pd.merge(train_raw,store_median_sales, on='Store', how='left')
	test = pd.merge(test_raw,store_median_sales,on='Store',how='left')
	print('store_median_sales added')

	#calculate dayofweek median sales per store
	store_dayofweek_median_sales = train.groupby(['Store','DayOfWeek'])[['Sales']].median().reset_index()

	store_dayofweek_median_sales.columns = ["Store","DayOfWeek","store_dayofweek_median_sales"]
	train = pd.merge(train,store_dayofweek_median_sales,on=["Store","DayOfWeek"],how="left")
	test = pd.merge(test,store_dayofweek_median_sales,on=["Store","DayOfWeek"],how="left")
	print('store_dayofweek_median_sales added')

	#calculate dayofweek median sales across all stores
	dayofweek_median_sales = train.groupby('DayOfWeek')[['Sales']].median().reset_index()
	dayofweek_median_sales.columns = ["DayOfWeek","dayofweek_median_sales"]
	train = pd.merge(train,dayofweek_median_sales,on="DayOfWeek",how="left")
	test = pd.merge(test,dayofweek_median_sales,on="DayOfWeek",how="left")
	print('dayofweek_median_sales added')

	#calculate sales/customer ratio
	sales_customer_ratio = (train['Sales']/train['Customers']).T
	train['sales_customer_ratio'] = sales_customer_ratio
	sales_customer_ratio_per_store = train.groupby('Store')[['sales_customer_ratio']].median().reset_index()

	train = pd.merge(train,sales_customer_ratio_per_store,on="Store",how="left")
	train = train.drop('sales_customer_ratio_x',axis = 1)
	train=train.rename(columns = {'sales_customer_ratio_y':'sales_customer_ratio'})
	test = pd.merge(test,sales_customer_ratio_per_store,on="Store",how="left")
	print('sales/customer ratio per store added')

	#school holiday median sales per store/non-school holiday median sales per store
	school_holiday_median_sales_per_store = train.groupby(['Store','SchoolHoliday'])[['Sales']].median().reset_index()
	non_school_holiday_median_sales_per_store = school_holiday_median_sales_per_store.loc[school_holiday_median_sales_per_store['SchoolHoliday'] == 0][['Store','Sales']]
	school_holiday_median_sales_per_store = school_holiday_median_sales_per_store.loc[school_holiday_median_sales_per_store['SchoolHoliday'] == 1][['Store','Sales']]
	non_school_holiday_median_sales_per_store.columns = ['Store','non_school_holiday_median_sales_per_store']
	school_holiday_median_sales_per_store.columns = ['Store','school_holiday_median_sales_per_store']
	train = pd.merge(train,non_school_holiday_median_sales_per_store,on="Store",how="left")
	train = pd.merge(train,school_holiday_median_sales_per_store,on="Store",how="left")
	test = pd.merge(test,non_school_holiday_median_sales_per_store,on="Store",how="left")
	test = pd.merge(test,school_holiday_median_sales_per_store,on="Store",how="left")
	print('school holiday median sales per store/non-school holiday median sales per store added')

	#school holiday median sales per store/non-school holiday median sales
	school_holiday_median_sales = train.groupby(['SchoolHoliday'])[['Sales']].median().reset_index()
	school_holiday_median_sales.columns = ['SchoolHoliday','SchoolHoliday_NonSchoolHoliday_median_sales']
	train = pd.merge(train,school_holiday_median_sales,on="SchoolHoliday",how="left")
	test = pd.merge(test,school_holiday_median_sales,on="SchoolHoliday",how="left")
	print('school holiday median sales per store/non-school holiday median sales added')
	#state holiday median sales per store/non-state holiday median sales per store
	state_holiday_median_sales_per_store = train.groupby(['Store','StateHoliday'])[['Sales']].median().reset_index()

	non_state_holiday_median_sales_per_store = state_holiday_median_sales_per_store.loc[state_holiday_median_sales_per_store['StateHoliday'] == '0'][['Store','Sales']]
	state_holiday_median_sales_per_store = state_holiday_median_sales_per_store.loc[state_holiday_median_sales_per_store['StateHoliday'] != '0'][['Store','Sales']]
	non_state_holiday_median_sales_per_store.columns = ['Store','non_state_holiday_median_sales_per_store']
	state_holiday_median_sales_per_store.columns = ['Store','state_holiday_median_sales_per_store']
	state_holiday_median_sales_per_store = state_holiday_median_sales_per_store.drop_duplicates(subset = 'Store')
	train = pd.merge(train,non_state_holiday_median_sales_per_store,on="Store",how="left")
	train = pd.merge(train,state_holiday_median_sales_per_store,on="Store",how="left")
	test = pd.merge(test,non_state_holiday_median_sales_per_store,on="Store",how="left")
	test = pd.merge(test,state_holiday_median_sales_per_store,on="Store",how="left")

	print('state holiday median sales per store/non-state holiday median sales per store added')

	#state holiday median sales/non-state holiday median sales
	state_holiday_median_sales = train.groupby(['StateHoliday'])[['Sales']].median().reset_index()
	state_holiday_median_sales.columns = ['StateHoliday','StateHoliday_NonStateHoliday_median_sales']
	train = pd.merge(train,state_holiday_median_sales,on="StateHoliday",how="left")
	test = pd.merge(test,state_holiday_median_sales,on="StateHoliday",how="left")
	print('state holiday median sales/non-state holiday median sales added')

	# average number of customers per day, numeric
	train_customer_per_day = train_raw.groupby('Date')[['Customers']].mean().reset_index()
	test_customer_per_day = test_raw.groupby('Date')[['Customers']].mean().reset_index()
	train_customer_per_day.columns = ["Date","Customers_per_day"]
	test_customer_per_day.columns = ["Date","Customers_per_day"]
	train = pd.merge(train,train_customer_per_day, on='Date', how='left')
	test = pd.merge(test,test_customer_per_day,on='Date',how='left')
	print('average cusomers per day added')

	# average number of customers per day of week, numeric
	train_customer_per_dow = train_raw.groupby('DayOfWeek')[['Customers']].mean().reset_index()
	test_customer_per_dow = test_raw.groupby('DayOfWeek')[['Customers']].mean().reset_index()
	train_customer_per_dow.columns = ["DayOfWeek","Customers_per_dow"]
	test_customer_per_dow.columns = ["DayOfWeek","Customers_per_dow"]
	train = pd.merge(train,train_customer_per_dow, on='DayOfWeek', how='left')
	test = pd.merge(test,test_customer_per_dow,on='DayOfWeek',how='left')
	print('average cusomers per day of week added')

	# average number of customers per store, numeric 
	train_customer_per_store = train_raw.groupby('Store')[['Customers']].mean().reset_index()
	test_customer_per_store = test_raw.groupby('Store')[['Customers']].mean().reset_index()
	train_customer_per_store.columns = ["Store","Customers_per_store"]
	test_customer_per_store.columns = ["Store","Customers_per_store"]
	train = pd.merge(train,train_customer_per_store, on='Store', how='left')
	test = pd.merge(test,test_customer_per_store,on='Store',how='left')
	print('average cusomers per store added')

	# average number of customer per state holiday, numeric 
	train_customer_per_state_holiday = train_raw.groupby('StateHoliday')[['Customers']].mean().reset_index()
	test_customer_per_state_holiday = test_raw.groupby('StateHoliday')[['Customers']].mean().reset_index()
	train_customer_per_state_holiday.columns = ["StateHoliday","Customers_per_state_holiday"]
	test_customer_per_state_holiday.columns = ["StateHoliday","Customers_per_state_holiday"]
	train = pd.merge(train,train_customer_per_state_holiday, on='StateHoliday', how='left')
	test = pd.merge(test,test_customer_per_state_holiday,on='StateHoliday',how='left')
	print('average cusomers per state holiday added')

	# average number of customer per school holiday, numeric 
	train_customer_per_school_holiday = train_raw.groupby('SchoolHoliday')[['Customers']].mean().reset_index()
	test_customer_per_school_holiday = test_raw.groupby('SchoolHoliday')[['Customers']].mean().reset_index()
	train_customer_per_school_holiday.columns = ["SchoolHoliday","Customers_per_school_holiday"]
	test_customer_per_school_holiday.columns = ["SchoolHoliday","Customers_per_school_holiday"]
	train = pd.merge(train,train_customer_per_school_holiday, on='SchoolHoliday', how='left')
	test = pd.merge(test,test_customer_per_school_holiday,on='SchoolHoliday',how='left')
	print('average cusomers per school holiday added')

	# date processing
	res_train = train_raw[['Date', 'StateHoliday', 'SchoolHoliday']]
	res_train['Holiday'] = np.where((res_train['SchoolHoliday'] == 0) & (res_train['StateHoliday']=="0"), 0, 1)
	res_train = res_train[["Date","Holiday"]]
	res_train = res_train.drop_duplicates()

	res_test = test_raw[['Date', 'StateHoliday', 'SchoolHoliday']]
	res_test['Holiday'] = np.where((res_test['SchoolHoliday'] == 0) & (res_test['StateHoliday']=="0"), 0, 1)
	res_test = res_test[["Date","Holiday"]]
	res_test = res_test.drop_duplicates()

	res_train.append(res_test)
	res_train = res_train[res_train.duplicated("Date") == False]

	res_train["next_7_day_holidays"] = 0
	res_train["next_3_day_have_holidays"] = 0
	res_train["next_2_day_have_holidays"] = 0
	res_train["next_1_day_have_holidays"] = 0

	dates = pd.to_datetime(res_train["Date"], infer_datetime_format  =True)
	dates_str = [d.date().strftime('%Y-%m-%d') for d in dates]

	for d in dates:
		# count number of holidays in next 7 days
		date_list = [d+datetime.timedelta(days=i+1) for i in range(7)] 
		date_list = [i.date().strftime('%Y-%m-%d') for i in date_list]
		count = 0
		for i in date_list:
			if i in dates_str:
				row = res_train.loc[res_train["Date"] == i]
				if int(row["Holiday"]) == 1:
					count += 1
		res_train["next_7_day_holidays"][res_train["Date"] == d.date().strftime('%Y-%m-%d')] = count

		# whether there is a binary in next 3/2/1 days/day, binary

		next_day = d + datetime.timedelta(days=1)
		next_day = next_day.date().strftime('%Y-%m-%d')
		if next_day in dates_str:
			row = res_train.loc[res_train["Date"] == next_day]
			if int(row["Holiday"]) == 1:
				res_train["next_1_day_have_holidays"][res_train["Date"] == d.date().strftime('%Y-%m-%d')] = 1

		next_2_days = date_list[:2]
		two_day_flag = 0
		for day in next_2_days:
			if day in dates_str:
				row = res_train.loc[res_train["Date"] == day]
				if int(row["Holiday"]) == 1:
					two_day_flag = 1

		next_3_days = date_list[:3]
		three_day_flag = 0
		for day in next_3_days:
			if day in dates_str:
				row = res_train.loc[res_train["Date"] == day]
				if int(row["Holiday"]) == 1:
					three_day_flag = 1

		if two_day_flag == 1:
			res_train["next_2_day_have_holidays"][res_train["Date"] == d.date().strftime('%Y-%m-%d')] = 1
		if three_day_flag == 1:
			res_train["next_3_day_have_holidays"][res_train["Date"] == d.date().strftime('%Y-%m-%d')] = 1

	#date features merge
	train = pd.merge(train,res_train,on="Date",how="left")
	test = pd.merge(test,res_train,on="Date",how="left")
	print("holiday related features added")


	train.to_csv(output_train_file_path,index=False)
	test.to_csv(output_test_file_path,index=False)

if __name__ == '__main__':
	print('start')
	process(INPUT_TRAIN_FILE_PATH,INPUT_TEST_FILE_PATH,OUTPUT_TRAIN_FILE_PATH,OUTPUT_TEST_FILE_PATH)
	print('complete')